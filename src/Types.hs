module Types where

-- | Rotation values as returned by the accelerometer.
data Orientation = RightUp | BottomUp | LeftUp | Normal | Undefined deriving (Show)

-- | Rotation direction.
data Direction = Clockwise | Counterclockwise

-- | Rotation represented by direction and number of steps.
data RotOffset = RotOffset { direction :: Direction, steps :: Int }

-- | Configuration for the application.
data HIIOConfig = HIIOConfig { offset :: RotOffset -- ^ Offset to the sensor data to correct if it is mounted sideways.
                             , output :: String -- ^ Output device to rotate.
                             , inputs :: [Int] -- ^ Input devices to rotate.
                             , runAsDaemon :: Bool -- ^ Whether to run continually or only once.
                             , flip :: Bool -- ^ Whether to flip the accelerometer data.
                             , verbose :: Bool -- ^ Whether to enable verbose output.
                             }
