module Conversions where

import Types

readOrientation :: String -> Orientation
readOrientation "right-up"  = RightUp
readOrientation "left-up"   = LeftUp
readOrientation "bottom-up" = BottomUp
readOrientation "normal"    = Normal
readOrientation _           = Undefined

orToRot :: Orientation -> Maybe String
orToRot RightUp   = return "left"
orToRot LeftUp    = return "right"
orToRot BottomUp  = return "inverted"
orToRot Normal    = return "normal"
orToRot Undefined = Nothing

orToMatrix :: Orientation -> Maybe String
orToMatrix RightUp   = return "0 -1 1 1 0 0 0 0 1"
orToMatrix LeftUp    = return "0 1 0 -1 0 1 0 0 1"
orToMatrix BottomUp  = return "-1 0 1 0 -1 1 0 0 1"
orToMatrix Normal    = return "1 0 0 0 1 0 0 0 1"
orToMatrix Undefined = Nothing

rotToOr :: String -> Orientation
rotToOr "left"     = RightUp
rotToOr "right"    = LeftUp
rotToOr "inverted" = BottomUp
rotToOr "normal"   = Normal
rotToOr _          = Undefined

flipOrientation :: Orientation -> Orientation
flipOrientation LeftUp = RightUp
flipOrientation RightUp = LeftUp
flipOrientation o = o
