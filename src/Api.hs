{-# LANGUAGE OverloadedStrings, FlexibleInstances, RecordWildCards #-}

module Api (Acc(..), FlowCmds(..), Xr(..)) where

import Control.Monad.Reader
import DBus
import DBus.Client
import System.Process
import System.Time.Extra

import Types
import Conversions

-- | API for accelerometer usage
class Acc m where
  hasAcc :: m Bool -- ^ Whether the machine has an accelerometer.
  claimAcc :: m () -- ^ Activates the accelerometer. Note that this call might return before the accelerometer produces meaningful values!
  releaseAcc :: m () -- ^ Deactivates the accelerometer.
  readAcc :: m Orientation -- ^ The current orientation of the machine.

-- | API for flow control int he application
class FlowCmds m where
  sleep' :: Seconds -> m () -- ^ Pause execution for passed number of seconds.
  repeat' :: Int -> m () -> m () -- ^ Repeat the passed action for the passed numbr of times.
  forever' :: m () -> m () -- ^ Repeat the passed action forever.
  print' :: Show t => t -> m () -- ^ Print the argument to the console.

-- | API for rotating
class Xr m where
  setScreenOrientation :: Orientation -> m ()
  setInputOrientations :: Orientation -> m ()

handleEither :: (Show e) => Either e r -> IO r
handleEither (Left e) = fail $ show e
handleEither (Right r) = return r


rotate' :: Direction -> Orientation -> Orientation
rotate' Counterclockwise Normal = RightUp
rotate' Counterclockwise RightUp = BottomUp
rotate' Counterclockwise BottomUp = LeftUp
rotate' Counterclockwise LeftUp = Normal
rotate' _ Undefined = Undefined
rotate' Clockwise o = let
  r = rotate' Counterclockwise
  in r . r . r $ o

-- | Rotates the given orientation by the given offset.
rotate :: RotOffset -> Orientation -> Orientation
rotate RotOffset{..} = let r = rotate' direction in
  case steps `mod` 4 of
    0 -> id
    1 -> r
    2 -> r . r
    3 -> r . r . r
    _otherwise -> id -- ignore all other values

mcd :: MemberName -> MethodCall
mcd f = (methodCall "/net/hadess/SensorProxy" "net.hadess.SensorProxy" f)
  { methodCallDestination = Just "net.hadess.SensorProxy"
  }

instance Acc (ReaderT (Client, HIIOConfig) IO) where
  hasAcc = ReaderT $ \(c, _) -> handleEither =<< getPropertyValue c (mcd "HasAccelerometer")
  releaseAcc = ReaderT $ \(c, _) -> fmap (const ()) $ handleEither =<< call c (mcd "ReleaseAccelerometer")
  claimAcc = ReaderT $ \(c, _) -> fmap (const ()) $ handleEither =<< call c (mcd "ClaimAccelerometer")
  readAcc = ReaderT $ \(c, HIIOConfig{..}) -> (handleEither . fmap (rotate offset . readOrientation)) =<< getPropertyValue c (mcd "AccelerometerOrientation")

instance FlowCmds IO where
  sleep' = sleep
  repeat' t act = if t >= 1 then act *> repeat' (t - 1) act else return ()
  print' = print
  forever' = forever

instance FlowCmds m => FlowCmds (ReaderT c m) where
  sleep' = ReaderT . const . sleep'
  repeat' t act = ReaderT $ \ c -> repeat' t (runReaderT act c)
  print' = ReaderT . const . print'
  forever' act  = ReaderT $ \ c -> forever' $ runReaderT act c

instance Xr (ReaderT (c, HIIOConfig) IO) where
  setScreenOrientation o = ReaderT $ \(_, HIIOConfig{..}) -> do
    rot <- maybe (fail "Invalid orientation") return $ orToRot $ if flip then flipOrientation o else o
    _ <- when verbose $ putStrLn $ "Rotating output " <> output <> " to " <> rot
    _ <- createProcess (shell $ "xrandr --output " <> output <> " --rotate " <> rot)
    return ()
  setInputOrientations o = ReaderT $ \(_, HIIOConfig{..}) -> do
    mat <- maybe (fail "Invalid orientation") return $ orToMatrix $ if flip then flipOrientation o else o
    _ <- traverse (\ input -> do
                      _ <- when verbose $ putStrLn $ "Rotating input " <> output <> " using matrix " <> mat
                      createProcess (shell $ "xinput set-prop " <> show input <> " 'Coordinate Transformation Matrix' " <> mat)) inputs
    return ()
