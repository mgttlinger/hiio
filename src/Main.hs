{-# LANGUAGE OverloadedStrings, FlexibleInstances #-}

module Main where

import Control.Monad.Reader
import DBus.Client
import Options.Applicative

import Types
import Api
-- * Program logic

-- | Reads the rotation from the accelerometer and sets screen and input devices accordingly.
setRot :: (Acc m, Xr m, Monad m) => m ()
setRot = do
    -- Get orietation
    r <- readAcc
    -- Set all orientations
    setScreenOrientation r
    setInputOrientations r

-- | Convenience function to execute the passed monad expression in a context where the accelerometer is initialised and handle the cleanup afterwards.
withAcc :: (Acc m, FlowCmds m, Monad m, MonadFail m) => m t -> m t
withAcc act = do
  -- Check if machine has accelerometer
  aval <- hasAcc
  if not aval then fail "Machine has no accelerometer!" else return ()
  -- Activate accelerometer
  claimAcc
  -- Wait for a while for data to become available
  sleep' 1
  r <- act
  -- Free accelerometer again
  releaseAcc
  return r

-- | Sets the rotation every second forever.
daemon :: (Acc m, FlowCmds m, Xr m, Monad m, MonadFail m) => m ()
daemon = withAcc $ forever' $ setRot *> sleep' 1
-- | Sets the rotation once.
once :: (Acc m,  FlowCmds m, Xr m, Monad m, MonadFail m) => m ()
once = withAcc setRot


-- | After parsing the cmd args, connects to the system DBus and then either runs 'once' or 'daemon' depending on the flags.
main :: IO ()
main = do
  cfg <- execParser args
  client <- connectSystem
  runReaderT (if runAsDaemon cfg then daemon else once) (client, cfg)

-- * Cmd argument handling

-- | HIIO cmd argument structure
args :: ParserInfo HIIOConfig
args = info (hiioConfig <**> helper) $ header "hiio - A utility to rotate screen and input from Dbus iio data (iio-sensor-proxy)"

-- | Rotational offset cmd option declaration
rotOffset :: Parser RotOffset
rotOffset = cw <|> ccw
  where
    cw = RotOffset Clockwise <$> option auto (long "cw" <> metavar "STEPS" <> help "Offset sensor data STEPS many steps clockwise")
    ccw = RotOffset Counterclockwise <$> option auto (long "ccw" <> metavar "STEPS" <> help "Offset sensor data STEPS many steps counterlockwise" <> value 0)

-- | Declaration of the cmd options to specify HIIOConfig
hiioConfig :: Parser HIIOConfig
hiioConfig = HIIOConfig
  <$> rotOffset
  <*> strOption (long "output" <>
                 short 'o' <>
                 metavar "OUTPUT" <>
                 help "xrandr output to rotate"
                )
  <*> option auto (long "inputs" <>
                 short 'i' <>
                 metavar "IDs" <>
                 help "xinput ids of the inputs to rotate. E.g. '[15, 16]'"
                )
  <*> switch (long "daemon" <>
              short 'd' <>
              help "run as a daemon"
             )
  <*> switch (long "flip" <>
              short 'f' <>
              help "flip the accelerometer data"
             )
  <*> switch (long "verbose" <>
              short 'v' <>
              help "enable verbose output"
             )
